﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace RazorVue {

    [UsedImplicitly]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract class VueModel : PageModel {

        public async Task OnGet() {
            await onGet();
        }


        public void OnPost() {
            onPost();
        }


        [UsedImplicitly]
        public abstract Task onGet();


        [UsedImplicitly]
        public virtual void onPost() { }

    }

}