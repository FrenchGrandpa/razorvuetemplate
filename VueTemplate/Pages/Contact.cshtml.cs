﻿using System;
using System.Threading.Tasks;
using RazorVue;


namespace VueTemplate.Pages {

    public class ContactModel : VueModel {

        public ContactModel(string message) {
            this.message = message;
        }


        public string message { get; }


        public override async Task onGet() {
            await Task.Run(() => Console.WriteLine("Hello"));
        }

    }

}