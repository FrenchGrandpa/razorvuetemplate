using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RazorVue;


namespace VueTemplate.Pages {

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class ErrorModel : VueModel {

        public string requestId { get; private set; }
        public bool showRequestId => !string.IsNullOrEmpty(requestId);


        public override async Task onGet() {
            await Task.Run(() => requestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier);
        }

    }

}