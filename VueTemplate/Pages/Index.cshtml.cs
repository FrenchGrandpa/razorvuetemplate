using System;
using System.Threading.Tasks;
using RazorVue;


namespace VueTemplate.Pages {

    public class IndexModel : VueModel {

        public override async Task onGet() {
            await Task.Run(() => Console.WriteLine("Hello"));
        }

    }

}