﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;


namespace VueTemplate {

    public static class Program {

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public static void Main(string[] args) {
            createWebHostBuilder(args).Build().Run();
        }


        private static IWebHostBuilder createWebHostBuilder(string[] args) {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        }

    }

}